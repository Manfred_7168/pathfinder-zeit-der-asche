## Version 6.6.2

- alles für das System PF2e v6.6.2 angepasst
- '@Template[type:'  in '@Template[' geändert
- '@Check[type:'  in '@Check['  geändert
- '|basic:true' in '|basic' geändert
- einige Fehlerhafte Links in Journalen angepasst