## Version 1.3.0

- Geänderte Akteure (wg. Änderung Translator im German-Modul)
- Geänderte Pfade der Portrait- und Tokenbilder
ACHTUNG: Damit die Akteure zu den geänderten Bildpfaden passen, muss das Abenteuer neu importiert werden!
- Einige Korrekturen an den Akteuren

## Version 1.3.1
- fehlendes Token für die Blutige Klinge (bloody-blade-mercenary.webp)
- Dateien pictures/age-of-ashes/tokens/accursed-forge-spurned.webp und pictures/age-of-ashes/tokens/belmazog.webp waren zu groß
