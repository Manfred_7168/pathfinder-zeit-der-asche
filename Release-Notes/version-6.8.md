## Version 6.8.5

- Journale überarbeitet (je Kapitel und Teil ein eigenes Journal mit Pages für Bereichsüberschriften und Räume)
- Journal-PINs auf Maps eingefügt
- Viele @Check\[...\] in \[\[/act ...\]\] geändert
Damit sollte v6.8.5 mit neuerem PF2e-System deutlich besser funktionieren.
