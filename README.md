# Pathfinder - Zeit der Asche

Dokumentation, Fehlermeldungen und Hilfen zum Foundry-Modul Zeit der Asche (pf2e-zda) von Ulisses Spiele.

## Anleitung

Wenn du das Modul "Zeit der Asche" zum ersten Mal in deiner Welt aktiviert hast, erscheint zunächst diese Anleitung.

Es gibt ein Abentuer-Kompendium im Modul. Dieses enthält 6 Abenteuer, jedes ein Kapitel von Zeit der Asche.

* Kapitel 1: Der Höllenritterhügel
* Kapitel 2: Kult der Asche
* Kapitel 3: Das Morgen muss brennen
* Kapitel 4: Feuer und Spuk
* Kapitel 5: Die Blutrote Triade
* Kapitel 6: Zerstörte Hoffnung

In jedem dieser Abenteuer gibt es einen Button zum Importieren des Abenteuers in deine Welt. Um die Ladezeit der Welt gering zu halten, empfehle ich nur 1-2 Kapitel zu importieren. Hast du das Kapitel gespielt, kannst du die entsprechenden Szenen und Akteure wieder löschen und das nächste Kapitel importieren.

Viel Spaß beim Spielen von Zeit der Asche auf Foundry VTT.

## Wie erhalte ich Hilfe bei der Benutzung?
Du kannst Hilfe über den Discord-Server von Ulisses im Kanal #pathfinder-2-vtt erhalten.

## Bekannte Fehler
In Kapitel 3 fehlt der Gefahr "Barzillais Hunde" die Angabe zur Bewegungsrate der Hunde. Diese ist 60 Fuß/18 m.

## Was mache ich, wenn mir ein Fehler auffällt?
Solltest du Fehler finden, erstelle doch bitte in diesem Projekt ein Issue.

## Release-Notes sind hier zu finden
https://gitlab.com/Manfred_7168/pathfinder-zeit-der-asche/-/blob/main/Release-Notes/version-1.3.md

